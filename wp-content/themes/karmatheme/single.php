<?php
    get_header();
    while(have_posts(  )) {
        the_post(  );
    ?>
    <div class="all-title-box">
		<div class="container text-center">
			<h1><?php the_title(  ) ?><span class="m_1">Keep Up with our news.</span></h1>
		</div>
    </div>

    <div id="overviews" class="section wb">
        <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 offset-md-2">
                    <p class="lead">Lorem Ipsum dolroin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem!</p>
                </div>
            </div><!-- end sub title -->

            <hr class="invis"> 
            <div class="row">
                <div class="metabox metabox--with-home-link">
                    <p><a class="metabox__blog-home-link" href="<?php echo site_url('/index.php/blog'); ?>"><i class="fa fa-home" aria-hidden="true"></i> Blog Home</a> <span class="metabox__main">Posted by <?php the_author_posts_link(); ?> on <?php the_time('d/m/Y'); ?> in <?php echo get_the_category_list(', '); ?></span></p>
                </div><!-- end metabox -->
            </div><!-- end row -->
            <div class="row">
                
                    <?php the_content(  );
                    }?>
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end overviews -->

    <hr class="invis"> 

<?php
    get_footer();
?>
    







