<?php
    get_header();
    while(have_posts(  )) {
        the_post(  );
    ?>
    <div class="all-title-box">
		<div class="container text-center">
			<h1><?php the_title(  ) ?><span class="m_1">Keep Up with our events.</span></h1>
		</div>
    </div>

    <div id="overviews" class="section wb">
        <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 offset-md-2">
                    <p class="lead">Lorem Ipsum dolroin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem!</p>
                </div>
            </div><!-- end sub title -->

            <hr class="invis"> 
            <div class="row">
                <div class="metabox metabox--with-home-link">
                    <p><a class="metabox__blog-home-link" href="<?php echo site_url('/index.php/events'); ?>"><i class="fa fa-home" aria-hidden="true"></i> Events Home</a> <span class="metabox__main"><?php the_title(  )?></span></p>
                </div><!-- end metabox -->
            </div><!-- end row -->
            <div class="row">
                
                    <?php the_content(  );
                    $relatedPrograms = get_field('related_programs');
                    if ($relatedPrograms) {
                        echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
                        echo '<br>';
                        echo '<h2 class="">Related Program(s)</h2>';
                        echo '<ul class="list-group">';
                        foreach($relatedPrograms as $program) { ?>
                          <li><a class="text-warning stretched-link" href="<?php echo get_the_permalink($program); ?>"><?php echo get_the_title($program); ?></a></li>
                        <?php }
                        echo '</ul>';
                        echo '</div>';
                    }
                    }?>
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end overviews -->

    <hr class="invis"> 

<?php
    get_footer();
?>
    







