<?php
    get_header();
    ?>
    <hr class="invis"> 


    <div id="testimonials" class="parallax section db parallax-off" style="background-image:url(<?php echo get_theme_file_uri("images/parallax_04.jpg");?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <div class="section-title text-center">
                        <h3>Latest Posts</h3>
                        <p>Lorem ipsum dolor sit aet, consectetur adipisicing lit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div><!-- end title -->

                    <div class="testi-carousel owl-carousel owl-theme">
                        <?php  $today = date('y/m/d');
                        $homepagePosts = new WP_Query(array(
                                    'posts_per_page' => 3,
                                    'orderby' => 'post_date',
                                    'order' => 'ASC',
                                ));

                        while ($homepagePosts->have_posts()) {
                            $homepagePosts->the_post(); ?>
                        <div class="testimonial clearfix">
							<div class="testi-meta">
                                <img src="<?php echo get_theme_file_uri("images/testi_02.png");?>" alt="" class="img-fluid">
                                <h4><?php the_author(  )?> </h4>
                            </div>
                                <div class="desc text-white">
                                    <h3><i class="fa fa-quote-left"></i> <?php the_title(  )?></h3>
                                    <span> <?php the_excerpt(  )?><a class="stretched-link text-warning" href="<?php the_permalink(  )?>">Read more</a></span>
                                </div>
                                <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->

                        <?php } wp_reset_postdata(  ) ?>
                       
                    </div><!-- end carousel -->
                </div><!-- end col -->

                <div class="col-md-2 col-sm-2"></div><!-- end col -->

                <div class="col-md-5 col-sm-5">
                    <div class="section-title text-center">
                        <h3>Latest Events</h3>
                        <p>Lorem ipsum dolor sit aet, consectetur adipisicing lit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                    </div><!-- end title -->

                    <div class="testi-carousel owl-carousel owl-theme">
                        <?php  $today = date('y/m/d');
                        $homepageEvents = new WP_Query(array(
                                    'posts_per_page' => 3,
                                    'post_type' => 'event',
                                    'meta_key' => 'event_date',
                                    'orderby' => 'meta_value',
                                    'order' => 'ASC',
                                    'meta_query' => array(
                                        array(
                                          'key' => 'event_date',
                                          'compare' => '>=',
                                          'value' => $today,
                                          'type' => 'DATE'
                                        )
                                    )
                                ));

                        while ($homepageEvents->have_posts()) {
                            $homepageEvents->the_post(); 
                            ?>
                            
                        <div class="testimonial clearfix">
							<div class="testi-meta">
                                <img src="<?php echo get_theme_file_uri("images/testi_01.png");?>" alt="" class="img-fluid">
                                <h4><?php the_author(  )?> </h4>
                            </div>
                                <div class="desc text-white">
                                    <h3><i class="fa fa-quote-left"></i> <?php the_title(  )?></h3>
                                    <span><?php the_excerpt(  )?><a class="stretched-link text-warning" href="<?php the_permalink(  )?>">Read more</a></span>
                                </div>
                                <!-- end testi-meta -->
                        </div>
                        <!-- end testimonial -->

                        <?php } wp_reset_postdata(  ) ?>
                       
                    </div><!-- end carousel -->
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end section -->
    <hr class="invis"> 

<?php
    get_footer();
?>
    