<?php
function school_files() {
  wp_enqueue_style('bootstrap_styles', get_template_directory_uri().'/css/bootstrap.min.css');
  wp_enqueue_style('font_awesome_styles', get_template_directory_uri().'/css/font-awesome.min.css');
  wp_enqueue_style('school_main_styles', get_stylesheet_uri());
  wp_enqueue_style('versions_styles', get_template_directory_uri().'/css/versions.css');
  wp_enqueue_style('responsive_styles', get_template_directory_uri().'/css/responsive.css');
  wp_enqueue_style('custome_styles', get_template_directory_uri().'/css/custom.css');
  wp_enqueue_script('modernizer_script', get_template_directory_uri().'/js/modernizer.js', NULL, '0.0.1', false);

  wp_enqueue_script('modernizer_script', '', NULL, '0.0.1', true);
  // wp_enqueue_script('jquery_script', get_template_directory_uri().'/js/jquery.min.js', '', '0.0.1', true);
  // wp_enqueue_script('bootstrap_twitter_script', get_template_directory_uri().'/js/bootstrap-twitter.min.js', '', '0.0.1', true);
  // wp_enqueue_script('jquery_touch_swipe_script', get_template_directory_uri().'/js/jquery-touch-swipe.min.js', '', '0.0.1', true);
  // wp_enqueue_script('bootstrap_touch_slider_script', get_template_directory_uri().'/js/bootstrap-touch-slider.js', NULL, '0.0.1', true);
  wp_enqueue_script('all_script', get_template_directory_uri().'/js/all.js', '', '0.0.1', true);
  wp_enqueue_script('custom_script', get_template_directory_uri().'/js/custom.js', NULL, '0.0.1', true);
  wp_enqueue_script('timeline_script', get_template_directory_uri().'/js/timeline.min.js', '', '0.0.1', true);
}

function postBanner($args = NULL)
{
  
  if (! $args['sizeName']) {
    $args['sizeName'] = 'defaultSizeImage';
  }
  if (! $args['thumbnailImage']) {
    if ( get_field( 'post_banner' ) )  {
      $sizeName = $args['sizeName'];
      $args['thumbnailImage'] = get_field( 'post_banner' )['sizes'][$sizeName];
    } else {
      // Assign here any default image
      $args['thumbnailImage'] = get_theme_file_uri( './images/blog_4.jpg' );
    }
  }
  ?>
    <div class="image-blog">
        <img src="<?php  
        echo  $args['thumbnailImage'];?>" alt="" class="img-fluid" >
    </div>
  <?php
}

function school_features()
{
  register_nav_menu( 'headerMenuLocation', 'Header Menu Location' );
  register_nav_menu( 'footerMenuLocation', 'Footer Menu Location' );
  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails' );
  add_image_size( 'professorLandscape', 400, 260, true );
  add_image_size( 'professorPortrait', 480, 650, true );
  add_image_size( 'postBanner', 290, 230, true );
  add_image_size( 'defaultSizeImage', 250, 250, true );
}

function adjust_queries($query)
{
  $today = date('y/m/d');
  if( !is_admin(  ) AND is_post_type_archive( 'event' ) AND $query->is_main_query(  )) {
    $query->set('meta_key', 'event_date');
    $query->set('orderby', 'meta_value');
    $query->set('order', 'ASC');
    $query->set('meta_query', array(
      array(
        'key' => 'event_date',
        'compare' => '>=',
        'value' => $today,
        'type' => 'DATE'
      )
  ));
  }
  if( !is_admin(  ) AND is_post_type_archive( 'program' ) AND $query->is_main_query(  )) {
    $query->set('posts_per_page', 5);
    $query->set('orderby', 'title');
    $query->set('order', 'ASC');

  }
}

function map_key ($api)
{
  $api['key'] = 'AIzaSyAFxx1NNFvr3poOOgwawEb35WYJIuvVxAQ';
  return $api;
}

add_action( 'wp_enqueue_scripts', 'school_files' );
add_action( 'after_setup_theme', 'school_features' );
add_action( 'pre_get_posts', 'adjust_queries' );
add_filter( 'acf/fields/google_map/api', 'map_key');