<?php
    get_header();
    ?>
    <div class="all-title-box">
		<div class="container text-center">
			<h1><?php the_title(  )?><span class="m_1"><?php the_archive_description(  )?></span></h1>
		</div>
    </div>

    <div id="overviews" class="section wb">
    <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 offset-md-2">
                    <p class="lead">Lorem Ipsum dolroin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem!</p>
                </div>
            </div><!-- end title -->

            <hr class="invis"> 

            <div class="row"> 
            <?php  $today = date('y/m/d');
                        $pastEvents = new WP_Query(array(
                                    'paged' => get_query_var( 'paged', 1 ),
                                    'posts_per_page' => 3,
                                    'post_type' => 'event',
                                    'meta_key' => 'event_date',
                                    'orderby' => 'meta_value',
                                    'order' => 'ASC',
                                    'meta_query' => array(
                                        array(
                                          'key' => 'event_date',
                                          'compare' => '<',
                                          'value' => $today,
                                          'type' => 'DATE'
                                        )
                                    )
                                ));
                            
                     while($pastEvents->have_posts(  )){
                                $pastEvents->the_post(  ); ?>
                    <div class="col-lg-6 col-md-6 col-12 pb-5">
                        <div class="course-item">
                            <div class="image-blog">
                                <img src="<?php echo get_theme_file_uri("images/blog_5.jpg")?>" alt="" class="img-fluid">
                            </div>
                            <div class="course-br">
                                <div class="course-title">
                                    <h2><a href="<?php the_permalink(  ) ?>" title=""><?php the_title(  )?></a></h2>
                                </div>
                                <div class="course-desc">
                                    <p><?php echo wp_trim_words( get_the_content(  ), 18 )?><a class="stretched-link text-warning" href="<?php the_permalink(  )?>"> Read more</a></p>
                                </div>
                                <div class="course-rating">
                                    4.5
                                    <i class="fa fa-star"></i>	
                                    <i class="fa fa-star"></i>	
                                    <i class="fa fa-star"></i>	
                                    <i class="fa fa-star"></i>	
                                    <i class="fa fa-star-half"></i>								
                                </div>
                            </div>
                            <div class="course-meta-bot">
                                <ul>
                                    <li><i class="fa fa-calendar" aria-hidden="true"></i> <?php the_field('event_date')?></li>
                                    <li><i class="fa fa-users" aria-hidden="true"></i> <?php the_field('event_type')?></li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- end col -->
                <?php } 
               ?>
            </div><!-- end row -->
			<?php echo paginate_links( array(
                'total' => $pastEvents->max_num_pages,  
                ) );?>
						
        </div><!-- end container -->
    </div>
    <hr class="invis"> 

<?php
    get_footer();
?>
    







