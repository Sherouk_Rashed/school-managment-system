<?php
    get_header();
    ?>
    <div class="all-title-box">
		<div class="container text-center">
			<h1><?php the_archive_title(  );?><span class="m_1"><?php the_archive_description(  )?></span></h1>
		</div>
    </div>

    <div id="overviews" class="section wb">
        <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 offset-md-2">
                    <p class="lead">Lorem Ipsum dolroin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem!</p>
                </div>
            </div><!-- end title -->

            <hr class="invis"> 

            <div class="row">
                <?php while(have_posts(  )){
                            the_post(  ); ?> 
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="blog-item">
                            <div class="image-blog">
                                <img src="<?php echo get_theme_file_uri("images/blog_1.jpg")?>" alt="" class="img-fluid">
                            </div>
                            <div class="meta-info-blog">
                                <span><i class="fa fa-calendar"></i> <?php the_time( "d/m/Y" ) ?></span>
                                <span><i class="fa fa-tag"></i>  <a href="#"><?php echo get_the_category_list(', ')?></a> </span>
                                <span><i class="fa fa-comments"></i> <a href="#">12 Comments</a></span>
                                <span><i class="fa fa-user"></i> Written By: <a href="#"><?php the_author_posts_link(  )?></a></span>
                            </div>
                            <div class="blog-title">
                                <h2><a href="<?php the_permalink(  )?>" title=""><?php the_title(  ) ?></a></h2>
                            </div>
                            <div class="blog-desc">
                                <p><?php the_excerpt(  ); ?></p>
                            </div>
                            <div class="blog-button">
                                <a class="hover-btn-new orange" href="<?php the_permalink(  );?>"><span>Read More<span></span></span></a>
                            </div>
                        </div>
                    </div><!-- end col -->
                <?php } 
                    echo paginate_links(  );
                ?>
            
            </div><!-- end row -->
        </div><!-- end container -->
    </div>
    <hr class="invis"> 

<?php
    get_footer();
?>
    







