<?php
?>
<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>About US</h3>
                        </div>
                        <p> Integer rutrum ligula eu dignissim laoreet. Pellentesque venenatis nibh sed tellus faucibus bibendum. Sed fermentum est vitae rhoncus molestie. Cum sociis natoque penatibus et magnis dis montes.</p>   
						<div class="footer-right">
							<ul class="footer-links-soi">
								<li><a href="#"><i class="fa fa-facebook mt-1"></i></a></li>
								<li><a href="#"><i class="fa fa-github mt-1"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter mt-1"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble mt-1"></i></a></li>
								<li><a href="#"><i class="fa fa-pinterest mt-1"></i></a></li>
							</ul><!-- end links -->
						</div>						
                    </div><!-- end clearfix -->
                </div><!-- end col -->

				<div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>Information Link</h3>
                        </div>
                        <ul class="footer-links">
                        <?php 
                                $menuLocations = get_nav_menu_locations();
                                $footerMenuID = $menuLocations['footerMenuLocation'];
                                $footerNavItems = wp_get_nav_menu_items($footerMenuID);
                                    if ($footerNavItems) {
                                        for ($i=0; $i < count($footerNavItems); $i++) {
                                            echo '<li><a href="'.$footerNavItems[$i]->url.'">'.$footerNavItems[$i]->title.'</a></li>';
                                        } 
                                    }
                                ?>
                        </ul><!-- end links -->
                    </div><!-- end clearfix -->
                </div><!-- end col -->
				
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="widget clearfix">
                        <div class="widget-title">
                            <h3>Contact Details</h3>
                        </div>

                        <ul class="footer-links">
                            <li><a href="mailto:#">info@yoursite.com</a></li>
                            <li><a href="#">www.yoursite.com</a></li>
                            <li>PO Box 16122 Collins Street West Victoria 8007 Australia</li>
                            <li>+61 3 8376 6284</li>
                        </ul><!-- end links -->
                    </div><!-- end clearfix -->
                </div><!-- end col -->
				
            </div><!-- end row -->
        </div><!-- end container -->
    </footer><!-- end footer -->
<?php wp_footer()?>
    <script>
		timeline(document.querySelectorAll('.timeline'), {
			forceVerticalMode: 700,
			mode: 'horizontal',
			verticalStartPosition: 'left',
			visibleItems: 4
		});
	</script>
</body>
</html>