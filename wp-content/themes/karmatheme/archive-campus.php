<?php
    get_header();
    ?>
    <div class="all-title-box">
		<div class="container text-center">
			<h1><?php the_archive_title(  );?><span class="m_1"><?php the_archive_description(  )?></span></h1>
		</div>
    </div>

    <div id="overviews" class="section wb">
    <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 offset-md-2">
                    <p class="lead">Lorem Ipsum dolroin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem!</p>
                </div>
            </div><!-- end title -->

            <hr class="invis"> 

            <div class="row"> 
                <?php while(have_posts(  )){
                                the_post(  ); ?>
                    <div class="col-lg-6 col-md-6 col-12 pb-5">
                        <div class="course-item">
                        
                        <div class="image-blog">

						<div  class="small-map" style="position: relative; overflow: hidden;">
                            <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                                <div class="gm-err-container">
                                    <div class="gm-err-content">
                                        <div class="gm-err-icon">
                                            <img src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" draggable="false" style="user-select: none;">
                                        </div>
                                        <div class="gm-err-title">Oops! Something went wrong.</div>
                                        <div class="gm-err-message">This page didn't load Google Maps correctly. See the JavaScript console for technical details.</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                            </div>
                            <div class="course-br">
                                <div class="course-title">
                                    <h2><a href="<?php the_permalink(  ) ?>" title=""><?php the_title(  )?></a></h2>
                                </div>
                                <div class="course-desc">
                                    <p><?php echo wp_trim_words( get_the_content(  ), 18 )?><a class="stretched-link text-warning" href="<?php the_permalink(  )?>"> Read more</a></p>
                                </div>
                            </div>
                            <div class="course-meta-bot">
                                <ul>
                                    <!-- <li><i class="fa fa-calendar" aria-hidden="true"></i> <?php the_field('event_date')?></li>
                                    <li><i class="fa fa-users" aria-hidden="true"></i> <?php the_field('event_type')?></li> -->
                                </ul>
                            </div>
                        </div>
                    </div><!-- end col -->
                <?php } ?>
            </div><!-- end row -->
            <!-- <p>Looking For Past Events? <a class="stretched-link text-warning" href=<?php echo site_url( '/index.php/past-events' )?> >Check Out Our past Events Archieve</a> </p> -->
						
        </div><!-- end container -->
    </div>
    <hr class="invis"> 

<?php
    get_footer();
?>
    







