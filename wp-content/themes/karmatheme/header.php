<?php 
?>
<!DOCTYPE>
<html>
    <head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href=<?php echo get_theme_file_uri("images/favicon.ico")?> type="image/x-icon" />
    <link rel="apple-touch-icon" href=<?php echo get_theme_file_uri("images/apple-touch-icon.png")?>>
        <?php wp_head() ?>
    </head>
    <body>
        <!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container-fluid">
				<a class="navbar-brand" href="index.html">
                    <img src="<?php echo get_theme_file_uri("images/logo.png");?>" alt="" />
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-host" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
					<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-host">
				<ul class="navbar-nav ml-auto">

					<?php 
					$menuLocations = get_nav_menu_locations();
					$headerMenuID = $menuLocations['headerMenuLocation'];
					$headerNavItems = wp_get_nav_menu_items($headerMenuID);
						if ($headerNavItems) {
							for ($i=0; $i < count($headerNavItems); $i++) {
								$content = '';
								$page_id = get_post_meta( $headerNavItems[$i]->ID, '_menu_item_object_id', true );
								$children = get_pages( array( 'child_of' => $page_id ) );
								global $wp_query;
								$post_obj = $wp_query->get_queried_object();
								$current_page_id = $post_obj->ID;
								if ( count( $children ) > 0 ) {
									if ($current_page_id == $page_id OR ($headerNavItems[$i]->title == ucfirst(get_post_type(  ).'s') OR ($headerNavItems[$i]->title == 'Events' AND is_page( 'past-events' )))){
										$content.='<li class="nav-item dropdown active">';
									}else{
										$content.='<li class="nav-item dropdown">';
									}
									$content.='<a class="nav-link" href="'.$headerNavItems[$i]->url.'" id="dropdown-a-'.$headerNavItems[$i]->title.'">'.$headerNavItems[$i]->title.'</a>
											<div class="dropdown-menu" aria-labelledby="dropdown-a">';
											foreach ($children as $child) {
												$content.='<a class="dropdown-item" href="'.get_permalink($child).'">'.get_the_title($child).'</a>';
											}
			
										$content.='</div></li>';
								}else {
									if ($current_page_id == $page_id OR ($headerNavItems[$i]->title == ucfirst(get_post_type(  ).'s') OR ($headerNavItems[$i]->title == 'Events' AND is_page( 'past-events' )))){
										$content.='<li class="nav-item active">';
									}else{
										$content.='<li class="nav-item">';
									}
									$content.='<a class="nav-link" href="'.$headerNavItems[$i]->url.'" title="'.$headerNavItems[$i]->title.'">'.$headerNavItems[$i]->title.'</a></li>';
								}
								echo $content;
							} 
						}
					?>
					</ul>
					<ul class="nav navbar-nav navbar-right">
                        <li><a class="hover-btn-new log orange" href="#" data-toggle="modal" data-target="#login"><span>Book Now</span></a></li>
                    </ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->
    