<?php
    get_header();
    while(have_posts(  )) {
        the_post(  );
    ?>
    <div class="all-title-box">
		<div class="container text-center">
			<h1><?php the_title(  ) ?><span class="m_1">Keep Up with All Our Programs.</span></h1>
		</div>
    </div>

    <div id="overviews" class="section wb">
        <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 offset-md-2">
                    <p class="lead">Lorem Ipsum dolroin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem!</p>
                </div>
            </div><!-- end sub title -->

            <hr class="invis"> 
            <div class="row">
                <div class="metabox metabox--with-home-link">
                    <p><a class="metabox__blog-home-link" href="<?php echo site_url('/index.php/programs'); ?>"><i class="fa fa-home" aria-hidden="true"></i> Programs Home</a> <span class="metabox__main"><?php the_title(  )?></span></p>
                </div><!-- end metabox -->
            </div><!-- end row -->
            <div class="">
                
                    <?php the_content(  );


          $relatedProfessors = new WP_Query(array(
            'posts_per_page' => -1,
            'post_type' => 'professor',
            'orderby' => 'title',
            'order' => 'ASC',
            'meta_query' => array(
              array(
                'key' => 'related_programs',
                'compare' => 'LIKE',
                'value' => '"'.get_the_ID(  ).'"',
              )
            )
          ));

          if ($relatedProfessors->have_posts()) {
            echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            echo '<hr>';
            echo '<br>';
            echo '<h2 class="">Related ' . get_the_title() . ' Proffesorss</h2>';
            echo '<br>';
            echo '<ul class="pl-0">';

            while($relatedProfessors->have_posts()) {
                $relatedProfessors->the_post(); ?>
                    <li class="professor-card__list-item">
                      <a class="text-warning stretched-link" href="<?php the_permalink(); ?>">
                        <img class="img-thumbnail professor-card__image" src="<?php the_post_thumbnail_url(  ) ?>">
                        <span><?php the_title(  )?></span>
                      </a>
                    </li>
                <?php } 
            }
            echo '</ul>';
            echo '</div>';


          wp_reset_postdata(  );
          $today = date('Ymd');
          $homepageEvents = new WP_Query(array(
            'posts_per_page' => 2,
            'post_type' => 'event',
            'meta_key' => 'event_date',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
              array(
                'key' => 'event_date',
                'compare' => '>=',
                'value' => $today,
                'type' => 'numeric'
              ),
              array(
                'key' => 'related_programs',
                'compare' => 'LIKE',
                'value' => '"'.get_the_ID(  ).'"',
              )
            )
          ));

          if ($homepageEvents->have_posts()) {
            echo '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            echo '<hr>';
            echo '<br>';
            echo '<h2 class="">Upcoming ' . get_the_title() . ' Events</h2>';
            echo '<br>';

            while($homepageEvents->have_posts()) {
                $homepageEvents->the_post(); ?>
                <div class="">
                <div class="">
                    <span class=""><?php
                    try {
                    $date = get_field('event_date');
                    $eventDate = new DateTime();
                    $eventDate->setTimestamp($date);
                    } catch (Exception $e) {
                        echo $date;
                        echo $e->getMessage();
                        exit(1);
                    }

                    ?></span>
                    <span class="badge-light"><?php echo $eventDate->format('M')." ".$eventDate->format('d') ?></span>  
                </a>
                <div class="">
                    <h5 class=""><a class="stretched-link text-warning h4" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    <p><?php if (has_excerpt()) {
                        echo get_the_excerpt();
                    } else {
                        echo wp_trim_words(get_the_content(), 18);
                        } ?> <a href="<?php the_permalink(); ?>" class="stretched-link text-warning"> Learn more</a></p>
                </div>
                </div>
                </div>
                <?php } 
            }
            echo '</div>';

        }?>
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end overviews -->

    <hr class="invis"> 

<?php
    get_footer();
?>
    







