<?php
    get_header();
    while(have_posts()){
        the_post();
        ?>
    <div id="overviews" class="section wb">
        <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 offset-md-2">
                    <h3><?php the_title()?></h3>
                </div>
            </div><!-- end title -->    
            
            <div class="row align-items-center">
            <?php $children = get_pages( array( 'child_of' => get_the_ID() ) );
                if ( is_page() && count( $children ) > 0 ) { ?>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                <?php }elseif ( is_page() && count( $children ) == 0 ) { ?>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <?php } ?>
                    <div class="d-inline message-box">
                    <?php
                        $theParent = wp_get_post_parent_id(get_the_ID());
                        if ($theParent) { ?>
                                <div class="metabox metabox--with-home-link">
                                    <p><a class="metabox__blog-home-link" href="<?php echo get_permalink($theParent); ?>"><i class="fa fa-home" aria-hidden="true"></i> Back to <?php echo get_the_title($theParent); ?></a><span class="metabox__main"><?php the_title(  ) ?></span></p>
                                </div><!-- end metabox -->
                            </p> 
                        <?php }
                        ?>
                        <p><?php the_content() ?></p>
                    </div><!-- end messagebox -->
                </div><!-- end col -->
                <?php if ( is_page() && count( $children ) > 0 ) { ?>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                <ul class="list-group">
                    <?php foreach ($children as $child) { ?>
                        <li class="list-group-item list-group-item-warning"><a href="<?php echo get_permalink($child); ?>"> Go to <?php echo get_the_title($child); ?></a></li>
                    <?php } ?>
        
                </ul>
                </div>
                <?php } ?>
            </div>
        </div><!-- end container -->
    </div><!-- end section -->
    <?php } 
        get_footer();
        ?>
    







